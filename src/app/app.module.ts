import { NgDompurifySanitizer } from '@tinkoff/ng-dompurify';
import {
  TUI_SANITIZER,
  TuiAlertModule,
  TuiButtonModule,
  TuiDialogModule,
  TuiDropdownModule,
  TuiRootModule
} from '@taiga-ui/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatrixComponent } from './matrix/matrix.component';
import { DateFormComponent } from './date-form/date-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  TuiInputDateModule,
  TuiUnfinishedValidatorModule
} from '@taiga-ui/kit';
import { TuiTableModule } from '@taiga-ui/addon-table';

@NgModule({
  declarations: [AppComponent, MatrixComponent, DateFormComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    TuiRootModule,
    TuiDialogModule,
    TuiAlertModule,
    TuiButtonModule,
    TuiInputDateModule,
    TuiUnfinishedValidatorModule,
    ReactiveFormsModule,
    TuiDropdownModule,
    TuiTableModule
  ],
  providers: [
    DateFormComponent,
    { provide: TUI_SANITIZER, useClass: NgDompurifySanitizer }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
