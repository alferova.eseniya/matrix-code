import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatrixNumber } from '../shared/interfaces/matrix.interface';
import { FormControl, FormGroup } from '@angular/forms';
import { TuiDay } from '@taiga-ui/cdk';
import {
  A_1,
  A_10,
  A_11,
  A_12,
  A_13,
  A_14,
  A_15,
  A_16,
  A_17,
  A_18,
  A_19,
  A_2,
  A_20,
  A_21,
  A_22,
  A_3,
  A_4,
  A_5,
  A_6,
  A_7,
  A_8,
  A_9
} from '../shared/descriptions/position_a.description';
import {
  B_1,
  B_10,
  B_11,
  B_12,
  B_13,
  B_14,
  B_15,
  B_16,
  B_17,
  B_18,
  B_19,
  B_2,
  B_20,
  B_21,
  B_22,
  B_3,
  B_4,
  B_5,
  B_6,
  B_7,
  B_8,
  B_9
} from '../shared/descriptions/position_b.description';
import {
  C_10,
  C_11,
  C_12,
  C_13,
  C_14,
  C_15,
  C_16,
  C_17,
  C_18,
  C_19,
  C_20,
  C_21,
  C_22,
  C_3,
  C_4,
  C_5,
  C_6,
  C_7,
  C_8,
  C_9
} from '../shared/descriptions/position_c.description';
import {
  E_10,
  E_11,
  E_12,
  E_13,
  E_14,
  E_16,
  E_18,
  E_20,
  E_22,
  E_3,
  E_4,
  E_5,
  E_6,
  E_7,
  E_8,
  E_9
} from '../shared/descriptions/position_e.description';

@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.less'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class MatrixComponent implements OnInit {
  // TO DO: убрать дату после тестирования
  birthDateForm = new FormGroup({
    birthday: new FormControl(new TuiDay(2017, 1, 15))
  });

  outer_numbers: MatrixNumber[] = [
    { position: 'B' },
    { position: 'G' },
    { position: 'C' },
    { position: 'H' },
    { position: 'D' },
    { position: 'I' },
    { position: 'A' },
    { position: 'F' }
  ];

  central_number: MatrixNumber = { position: 'E' };

  readonly columns = ['plus', 'minus'];

  constructor() {}

  ngOnInit(): void {
    console.log(this.outer_numbers);
  }

  countMatrix(day: string, month: number, year: string) {
    let A, C, D, F, E, G, H, I;
    if (Number(day) > 22) A = Number(day[0]) + Number(day[1]);
    else A = Number(day);
    this.outer_numbers[6].num = A;

    this.outer_numbers[0].num = month;

    C = Number(year[0]) + Number(year[1]) + Number(year[2]) + Number(year[3]);
    if (C > 22) C = this.simplify(C.toString());
    this.outer_numbers[2].num = C;

    D = A + month + C;
    if (D > 22) D = this.simplify(D.toString());
    this.outer_numbers[4].num = D;

    F = A + month;
    if (F > 22) F = this.simplify(F.toString());
    this.outer_numbers[7].num = F;

    G = month + C;
    if (G > 22) G = this.simplify(G.toString());
    this.outer_numbers[1].num = G;

    H = C + D;
    if (H > 22) H = this.simplify(H.toString());
    this.outer_numbers[3].num = H;

    I = D + A;
    if (I > 22) I = this.simplify(I.toString());
    this.outer_numbers[5].num = I;

    E = A + month + C + D;
    if (E > 22) E = this.simplify(E.toString());
    this.central_number.num = E;
  }

  simplify(string: string): number {
    let updNumber = Number(string[0]) + Number(string[1]);
    if (updNumber > 22) this.simplify(updNumber.toString());
    else return Number(string[0]) + Number(string[1]);

    return 0;
  }

  updateDate() {
    let birthDay: TuiDay = this.birthDateForm.value.birthday;
    this.countMatrix(
      birthDay.day.toString(),
      birthDay.month + 1,
      birthDay.year.toString()
    );
  }

  showTextOuterCircle(num: MatrixNumber) {
    switch (num.position) {
      case 'A':
        this.getMeaningForPositionA(num);
        break;
      case 'B':
        this.getMeaningForPositionB(num);
        break;
      case 'C':
        this.getMeaningForPositionC(num);
        break;
      default:
        num.meaning_positive = ['loading'];
        num.meaning_negative = ['loading'];
    }
    return num;
  }

  showTextCentralCircle(num: MatrixNumber) {
    this.getMeaningForPositionE(num);
    return num;
  }

  getMeaningForPositionA(num: MatrixNumber) {
    switch (num.num) {
      case 1:
        num.meaning_positive = A_1.meaning_positive;
        num.meaning_negative = A_1.meaning_negative;
        break;
      case 2:
        num.meaning_positive = A_2.meaning_positive;
        num.meaning_negative = A_2.meaning_negative;
        break;
      case 3:
        num.meaning_positive = A_3.meaning_positive;
        num.meaning_negative = A_3.meaning_negative;
        break;
      case 4:
        num.meaning_positive = A_4.meaning_positive;
        num.meaning_negative = A_4.meaning_negative;
        break;
      case 5:
        num.meaning_positive = A_5.meaning_positive;
        num.meaning_negative = A_5.meaning_negative;
        break;
      case 6:
        num.meaning_positive = A_6.meaning_positive;
        num.meaning_negative = A_6.meaning_negative;
        break;
      case 7:
        num.meaning_positive = A_7.meaning_positive;
        num.meaning_negative = A_7.meaning_negative;
        break;
      case 8:
        num.meaning_positive = A_8.meaning_positive;
        num.meaning_negative = A_8.meaning_negative;
        break;
      case 9:
        num.meaning_positive = A_9.meaning_positive;
        num.meaning_negative = A_9.meaning_negative;
        break;
      case 10:
        num.meaning_positive = A_10.meaning_positive;
        num.meaning_negative = A_10.meaning_negative;
        break;
      case 11:
        num.meaning_positive = A_11.meaning_positive;
        num.meaning_negative = A_11.meaning_negative;
        break;
      case 12:
        num.meaning_positive = A_12.meaning_positive;
        num.meaning_negative = A_12.meaning_negative;
        break;
      case 13:
        num.meaning_positive = A_13.meaning_positive;
        num.meaning_negative = A_13.meaning_negative;
        break;
      case 14:
        num.meaning_positive = A_14.meaning_positive;
        num.meaning_negative = A_14.meaning_negative;
        break;
      case 15:
        num.meaning_positive = A_15.meaning_positive;
        num.meaning_negative = A_15.meaning_negative;
        break;
      case 16:
        num.meaning_positive = A_16.meaning_positive;
        num.meaning_negative = A_16.meaning_negative;
        break;
      case 17:
        num.meaning_positive = A_17.meaning_positive;
        num.meaning_negative = A_17.meaning_negative;
        break;
      case 18:
        num.meaning_positive = A_18.meaning_positive;
        num.meaning_negative = A_18.meaning_negative;
        break;
      case 19:
        num.meaning_positive = A_19.meaning_positive;
        num.meaning_negative = A_19.meaning_negative;
        break;
      case 20:
        num.meaning_positive = A_20.meaning_positive;
        num.meaning_negative = A_20.meaning_negative;
        break;
      case 21:
        num.meaning_positive = A_21.meaning_positive;
        num.meaning_negative = A_21.meaning_negative;
        break;
      case 22:
        num.meaning_positive = A_22.meaning_positive;
        num.meaning_negative = A_22.meaning_negative;
        break;
      default:
        num.meaning_positive = ['вы в танцах'];
        num.meaning_negative = ['вы самое слабое звено'];
        break;
    }
    return num;
  }

  getMeaningForPositionB(num: MatrixNumber) {
    switch (num.num) {
      case 1:
        num.meaning_positive = B_1.meaning_positive;
        num.meaning_negative = B_1.meaning_negative;
        break;
      case 2:
        num.meaning_positive = B_2.meaning_positive;
        num.meaning_negative = B_2.meaning_negative;
        break;
      case 3:
        num.meaning_positive = B_3.meaning_positive;
        num.meaning_negative = B_3.meaning_negative;
        break;
      case 4:
        num.meaning_positive = B_4.meaning_positive;
        num.meaning_negative = B_4.meaning_negative;
        break;
      case 5:
        num.meaning_positive = B_5.meaning_positive;
        num.meaning_negative = B_5.meaning_negative;
        break;
      case 6:
        num.meaning_positive = B_6.meaning_positive;
        num.meaning_negative = B_6.meaning_negative;
        break;
      case 7:
        num.meaning_positive = B_7.meaning_positive;
        num.meaning_negative = B_7.meaning_negative;
        break;
      case 8:
        num.meaning_positive = B_8.meaning_positive;
        num.meaning_negative = B_8.meaning_negative;
        break;
      case 9:
        num.meaning_positive = B_9.meaning_positive;
        num.meaning_negative = B_9.meaning_negative;
        break;
      case 10:
        num.meaning_positive = B_10.meaning_positive;
        num.meaning_negative = B_10.meaning_negative;
        break;
      case 11:
        num.meaning_positive = B_11.meaning_positive;
        num.meaning_negative = B_11.meaning_negative;
        break;
      case 12:
        num.meaning_positive = B_12.meaning_positive;
        num.meaning_negative = B_12.meaning_negative;
        break;
      case 13:
        num.meaning_positive = B_13.meaning_positive;
        num.meaning_negative = B_13.meaning_negative;
        break;
      case 14:
        num.meaning_positive = B_14.meaning_positive;
        num.meaning_negative = B_14.meaning_negative;
        break;
      case 15:
        num.meaning_positive = B_15.meaning_positive;
        num.meaning_negative = B_15.meaning_negative;
        break;
      case 16:
        num.meaning_positive = B_16.meaning_positive;
        num.meaning_negative = B_16.meaning_negative;
        break;
      case 17:
        num.meaning_positive = B_17.meaning_positive;
        num.meaning_negative = B_17.meaning_negative;
        break;
      case 18:
        num.meaning_positive = B_18.meaning_positive;
        num.meaning_negative = B_18.meaning_negative;
        break;
      case 19:
        num.meaning_positive = B_19.meaning_positive;
        num.meaning_negative = B_19.meaning_negative;
        break;
      case 20:
        num.meaning_positive = B_20.meaning_positive;
        num.meaning_negative = B_20.meaning_negative;
        break;
      case 21:
        num.meaning_positive = B_21.meaning_positive;
        num.meaning_negative = B_21.meaning_negative;
        break;
      case 22:
        num.meaning_positive = B_22.meaning_positive;
        num.meaning_negative = B_22.meaning_negative;
        break;
      default:
        num.meaning_positive = ['вы в танцах'];
        num.meaning_negative = ['вы самое слабое звено'];
        break;
    }
    return num;
  }

  getMeaningForPositionC(num: MatrixNumber) {
    switch (num.num) {
      case 3:
        num.meaning_positive = C_3.meaning_positive;
        num.meaning_negative = C_3.meaning_negative;
        break;
      case 4:
        num.meaning_positive = C_4.meaning_positive;
        num.meaning_negative = C_4.meaning_negative;
        break;
      case 5:
        num.meaning_positive = C_5.meaning_positive;
        num.meaning_negative = C_5.meaning_negative;
        break;
      case 6:
        num.meaning_positive = C_6.meaning_positive;
        num.meaning_negative = C_6.meaning_negative;
        break;
      case 7:
        num.meaning_positive = C_7.meaning_positive;
        num.meaning_negative = C_7.meaning_negative;
        break;
      case 8:
        num.meaning_positive = C_8.meaning_positive;
        num.meaning_negative = C_8.meaning_negative;
        break;
      case 9:
        num.meaning_positive = C_9.meaning_positive;
        num.meaning_negative = C_9.meaning_negative;
        break;
      case 10:
        num.meaning_positive = C_10.meaning_positive;
        num.meaning_negative = C_10.meaning_negative;
        break;
      case 11:
        num.meaning_positive = C_11.meaning_positive;
        num.meaning_negative = C_11.meaning_negative;
        break;
      case 12:
        num.meaning_positive = C_12.meaning_positive;
        num.meaning_negative = C_12.meaning_negative;
        break;
      case 13:
        num.meaning_positive = C_13.meaning_positive;
        num.meaning_negative = C_13.meaning_negative;
        break;
      case 14:
        num.meaning_positive = C_14.meaning_positive;
        num.meaning_negative = C_14.meaning_negative;
        break;
      case 15:
        num.meaning_positive = C_15.meaning_positive;
        num.meaning_negative = C_15.meaning_negative;
        break;
      case 16:
        num.meaning_positive = C_16.meaning_positive;
        num.meaning_negative = C_16.meaning_negative;
        break;
      case 17:
        num.meaning_positive = C_17.meaning_positive;
        num.meaning_negative = C_17.meaning_negative;
        break;
      case 18:
        num.meaning_positive = C_18.meaning_positive;
        num.meaning_negative = C_18.meaning_negative;
        break;
      case 19:
        num.meaning_positive = C_19.meaning_positive;
        num.meaning_negative = C_19.meaning_negative;
        break;
      case 20:
        num.meaning_positive = C_20.meaning_positive;
        num.meaning_negative = C_20.meaning_negative;
        break;
      case 21:
        num.meaning_positive = C_21.meaning_positive;
        num.meaning_negative = C_21.meaning_negative;
        break;
      case 22:
        num.meaning_positive = C_22.meaning_positive;
        num.meaning_negative = C_22.meaning_negative;
        break;

      default:
        num.meaning_positive = ['вы в танцах'];
        num.meaning_negative = ['вы самое слабое звено'];
        break;
    }
    return num;
  }

  getMeaningForPositionE(num: MatrixNumber) {
    switch (num.num) {
      case 3:
        num.meaning_positive = E_3.meaning_positive;
        num.meaning_negative = E_3.meaning_negative;
        break;
      case 4:
        num.meaning_positive = E_4.meaning_positive;
        num.meaning_negative = E_4.meaning_negative;
        break;
      case 5:
        num.meaning_positive = E_5.meaning_positive;
        num.meaning_negative = E_5.meaning_negative;
        break;
      case 6:
        num.meaning_positive = E_6.meaning_positive;
        num.meaning_negative = E_6.meaning_negative;
        break;
      case 7:
        num.meaning_positive = E_7.meaning_positive;
        num.meaning_negative = E_7.meaning_negative;
        break;
      case 8:
        num.meaning_positive = E_8.meaning_positive;
        num.meaning_negative = E_8.meaning_negative;
        break;
      case 9:
        num.meaning_positive = E_9.meaning_positive;
        num.meaning_negative = E_9.meaning_negative;
        break;
      case 10:
        num.meaning_positive = E_10.meaning_positive;
        num.meaning_negative = E_10.meaning_negative;
        break;
      case 11:
        num.meaning_positive = E_11.meaning_positive;
        num.meaning_negative = E_11.meaning_negative;
        break;
      case 12:
        num.meaning_positive = E_12.meaning_positive;
        num.meaning_negative = E_12.meaning_negative;
        break;
      case 13:
        num.meaning_positive = E_13.meaning_positive;
        num.meaning_negative = E_13.meaning_negative;
        break;
      case 14:
        num.meaning_positive = E_14.meaning_positive;
        num.meaning_negative = E_14.meaning_negative;
        break;
      case 16:
        num.meaning_positive = E_16.meaning_positive;
        num.meaning_negative = E_16.meaning_negative;
        break;
      case 18:
        num.meaning_positive = E_18.meaning_positive;
        num.meaning_negative = E_18.meaning_negative;
        break;
      case 20:
        num.meaning_positive = E_20.meaning_positive;
        num.meaning_negative = E_20.meaning_negative;
        break;
      case 22:
        num.meaning_positive = E_22.meaning_positive;
        num.meaning_negative = E_22.meaning_negative;
        break;
      default:
        num.meaning_positive = ['вы в танцах'];
        num.meaning_negative = ['вы самое слабое звено'];
        break;
    }
    return num;
  }
}
