export interface MatrixNumber {
  num?: number;
  position: string;
  meaning_positive?: string[];
  meaning_negative?: string[];
}
