import { MatrixNumber } from '../interfaces/matrix.interface';

export const B_1: MatrixNumber = {
  num: 1,
  position: 'B',
  meaning_positive: [
    'Идеи',
    'Новизна',
    'Лидерство',
    'Общение',
    'Чувство превосходства',
    'Стремление к неизведанному',
    'Желание начать что-то новое'
  ],
  meaning_negative: [
    'Рутина',
    'Однообразие',
    'Отсутствие вариантов выбора',
    'Нерешительность',
    'Слабая воля',
    'Сокрытие идеи',
    'Зависть, чувство ущербности в сравнении с другими'
  ]
};

export const B_2: MatrixNumber = {
  num: 2,
  position: 'B',
  meaning_positive: [
    'Тайна и мистика',
    'Теории заговора',
    'Быть частью интриги или глобального процесса (быть знаменитостью, в центре внимания)'
  ],
  meaning_negative: [
    'Распространять сплетни и слухи',
    'Наговаривать на других',
    'Оценивать и осуждать чужие успехи',
    'Ложь и обман',
    'Гонка за молодостью'
  ]
};

export const B_3: MatrixNumber = {
  num: 3,
  position: 'B',
  meaning_positive: [
    'Красивые вещи',
    'Комфорт',
    'Женщины и дети',
    'Взращивание и развитие «проекта»',
    'Уход за собой, за домом, за садом',
    'Изысканное хобби (музыка, создание красивых вещей)'
  ],
  meaning_negative: [
    'Зацикленность на вещах',
    'Уродство, грязь, беспорядок',
    'Агрессия по отношению к детям и животным',
    'Борьба за выживание',
    'Отсутствие комфорта',
    'Отсутствие дела'
  ]
};

export const B_4: MatrixNumber = {
  num: 4,
  position: 'B',
  meaning_positive: [
    'Превосходство (лидерская позиция)',
    'Связь с отцом и сыном',
    'Ответственность за других',
    'Брутальность и мощь',
    'Мужские вещи, действия',
    'Экстрим',
    'Финансовая стабильность',
    'Большие пространства и размеры',
    'Наличие своего имущества (квартиры, дома)'
  ],
  meaning_negative: [
    'Подчинение',
    'Отсутствие отношений с отцом и сыном',
    'Зависимость от обстоятельств вопреки желанию',
    'Слабость',
    'Маленькие пространства и размеры',
    'Жизнь за счет других',
    'Отсутствие собственности',
    'Неспособность решить вопросы'
  ]
};

export const B_5: MatrixNumber = {
  num: 5,
  position: 'B',
  meaning_positive: [
    'Вера',
    'Традиции',
    'Знания',
    'Учение и передача знаний',
    'Активная позиция',
    'Общество как семья (группа единомышленников, религиозная община, родственники)'
  ],
  meaning_negative: [
    'Отсутствие веры',
    'Лень обучаться и открыться знаниям',
    'Диеты, религиозные посты, испытания на верность',
    'Неприятие чужого мнения',
    'Отсутствие ответа на вопрос'
  ]
};

export const B_6: MatrixNumber = {
  num: 6,
  position: 'B',
  meaning_positive: [
    'Активность, отдых и развлечения',
    'Любовь',
    'Новые знакомства',
    'Интриги и сплетни',
    'Флирт и игривость в общении',
    'Любовь к красивым дорогим вещам',
    'Значимость себя в обществе',
    'Знакомства с исключительными людьми (знаменитостями)',
    'Стремление к тактильному контакту (все воспринимается на ощупь)'
  ],
  meaning_negative: [
    'Осознание собственной ненужности',
    'Отсутствие внимания к себе',
    'Необходимость сделать выбор или принять решение',
    'Нелюбовь к себе',
    'Совершение рационального выбора, а не по желанию сердца',
    'Вид некрасивого и неухоженного человека',
    'Ощущение физического дискомфорта',
    'Ощущение невыносимости одиночества'
  ]
};

export const B_7: MatrixNumber = {
  num: 7,
  position: 'B',
  meaning_positive: [
    'Скорость',
    'Наличие цели и идеи',
    'Стремление вдохновлять других на действия',
    'Быстрые средства передвижения (транспорт)',
    'Спорт',
    'Достижения'
  ],
  meaning_negative: [
    'Невозможность быть капитаном и рулить ситуацией',
    'Страх транспорта и скорости',
    'Обездвиженность',
    'Слабость силы воли'
  ]
};

export const B_8: MatrixNumber = {
  num: 8,
  position: 'B',
  meaning_positive: [
    'Четкость и структурированность процессов',
    'Ясность жизни (чтобы все было понятно без контекстов и поиска дополнительных смыслов]',
    'Системность',
    'Закон и порядок',
    'Официальность'
  ],
  meaning_negative: [
    'Отсутствие конкретики и ясности',
    'Несправедливость',
    'Разногласие с законом',
    'Хаос (в мыслях, в действиях, в поступках)',
    'Неоднозначность',
    'Фиктивность',
    'Вынужденное встраивание в новую систему (новая работа, сфера деятельности, новый социум)'
  ]
};

export const B_9: MatrixNumber = {
  num: 9,
  position: 'B',
  meaning_positive: [
    'Знания\n' + 'Уединение (побыть наедине с самим собой)',
    'Записывание своих мыслей',
    'Погружение в интересную тему, чтобы докопаться до истоков',
    'Независимость (от людей, обстоятельств)',
    'Простота вещей',
    'Фундаментальность'
  ],
  meaning_negative: [
    'Быть в толпе',
    'Глупость окружения',
    'Поверхностность',
    'Зависимость (от людей и обстоятельств)',
    'Разочарование',
    'Страх одиночества',
    'Устои и рамки общества',
    'Необходимость решать бытовые вопросы',
    'Сомнения в своих возможностях (компетенции, правоте взглядов)'
  ]
};

export const B_10: MatrixNumber = {
  num: 10,
  position: 'B',
  meaning_positive: [
    'Вера в Предопределенность',
    'Жизнь без графиков и временных ограничений',
    'Спонтанность'
  ],
  meaning_negative: [
    'Графики и планы',
    'Соответствие нормам и требованиям',
    'Быть, как все'
  ]
};

export const B_11: MatrixNumber = {
  num: 11,
  position: 'B',
  meaning_positive: [
    'Масштабность',
    'Жизнь на широкую ногу',
    'Социальная активность',
    'Большие пространства',
    'Возможность проявить свою силу (физическую и моральную)',
    'Быть во главе большого процесса'
  ],
  meaning_negative: [
    'Бессилие и слабость',
    'Отсутствие значимости и причастности к чему-либо',
    'Тирания над слабыми',
    'Подчинение',
    'Трусость',
    'Отказ от ответственности',
    'Теснота',
    'Ограниченность возможностей',
    'Статичность'
  ]
};

export const B_12: MatrixNumber = {
  num: 12,
  position: 'B',
  meaning_positive: [
    'Способность видеть мир под разными углами',
    'Чувство полезности себя и своих действий',
    'Благотворительность',
    'Открытость и чистота души'
  ],
  meaning_negative: [
    'Вынужденность жертвовать собой во благо чужих интересов',
    'Чувство своей бесполезности',
    'Зависание в проблеме',
    'Зависть'
  ]
};

export const B_13: MatrixNumber = {
  num: 13,
  position: 'B',
  meaning_positive: [
    'Все новое',
    'Минимализм',
    'Обнуление',
    'Изменения',
    'Оригинальность',
    'Уникальность',
    'Приключения'
  ],
  meaning_negative: [
    'Хлам',
    'Рутина и быт',
    'Однообразие',
    'Ограниченность',
    'Старые вещи',
    'Жизнь - болото'
  ]
};

export const B_14: MatrixNumber = {
  num: 14,
  position: 'B',
  meaning_positive: [
    'Творчество',
    'Гармония, баланс',
    'Домашнее хозяйство, быт',
    'Еда и кулинария',
    'Спокойствие и размеренность',
    'Тихая, скромная жизнь',
    'Достаток без излишеств',
    'Стабильность'
  ],
  meaning_negative: [
    'Экстрим',
    'Зависть',
    'Лень',
    'Недостаток средств',
    'Жадность',
    'Нарушение баланса',
    'Неуемность',
    'Нарушение привычного ритма жизни'
  ]
};

export const B_15: MatrixNumber = {
  num: 15,
  position: 'B',
  meaning_positive: [
    'Мирские удовольствия',
    '«Секс, наркотики, рок-н-ролл»',
    'Возможность «выгуливать» своего внутреннего Дьявола',
    'Роскошная жизнь',
    'Деньги',
    'Ночная жизнь',
    'Манипуляции',
    '«Запретные дела» (чувство, когда выходишь за грань дозволенного в любой сфере)'
  ],
  meaning_negative: [
    'Притворство',
    'Поведение вопреки внутреннему самоощущению',
    'Стремление стать «как все»',
    'Перфекционизм',
    'Вынужденное бегство от собственного «Я» (самоотвращение)',
    'Зависимости'
  ]
};

export const B_16: MatrixNumber = {
  num: 16,
  position: 'B',
  meaning_positive: [
    'Трудности и сложные ситуации',
    'Экстремальные условия',
    'Приключения на свою голову',
    'Высота и воздух',
    'То, что заведомо может сделать сильнее',
    'Построение планов'
  ],
  meaning_negative: [
    'Боязнь высоты',
    'Привязка к материальному',
    'Озлобленность на жизнь и людей',
    'Потеря фундамента (земли под ногами)',
    'Страх краха и неудачи',
    'Сомнения и опасения в правильности действий'
  ]
};

export const B_17: MatrixNumber = {
  num: 17,
  position: 'B',
  meaning_positive: [
    'Популярность (звездность)',
    'Реализация своего таланта',
    'Профессионализм',
    'Всеобщее признание',
    'Высокое положение',
    'Наличие поклонников',
    'Яркая, насыщенная жизнь'
  ],
  meaning_negative: [
    'Незаметность (быть серой мышью)',
    'Нереализованность таланта',
    'Недостаточный уровень профессионализма',
    'Безликость в обществе',
    'Жизнь, лишенная событий'
  ]
};

export const B_18: MatrixNumber = {
  num: 18,
  position: 'B',
  meaning_positive: [
    'Лунные циклы',
    'Ночь (ночное время)',
    'Познание неизведанного',
    'Психология',
    'Природа (погружение в стихии)',
    'Метафизика',
    'Волшебство, магия'
  ],
  meaning_negative: [
    'Иллюзии и самообман',
    'Страхи',
    'Попытки быть «как все»',
    'Бегство от реальности',
    'Самокопание',
    'Боязнь непознанного и неизве',
    'Ломание внутренних ритмов'
  ]
};

export const B_19: MatrixNumber = {
  num: 19,
  position: 'B',
  meaning_positive: [
    'Богатство и наличие денег',
    'Возможность баловать подарками других людей',
    'Популярность и известность',
    'Пример для других',
    'Нахождение в центре событий',
    'Масштабные проекты',
    'Все большое',
    'Предметы роскоши и богатства',
    'Свет (в прямом смысле)'
  ],
  meaning_negative: [
    'Нищета и необходимость жить «по средствам»',
    'Скупость',
    'Обида и горечь прошлого',
    'Зацикленность на себе',
    'Отсутствие внимания со стороны других людей',
    'Нереализованность (планов, затей, желаний)',
    'Отсутствие возможности проявить себя'
  ]
};

export const B_20: MatrixNumber = {
  num: 20,
  position: 'B',
  meaning_positive: [
    'Активная гражданская позиция',
    'Причастность к судьбе семьи, Рода и родины',
    'Благотворительность и общественная деятельность',
    'Дети',
    'Увлечения и занятия из детства',
    'Семья, Род, родина',
    'Ценности и традиции'
  ],
  meaning_negative: [
    'Эмиграция и отлучение от родины',
    'Проблемы в семье или с родственниками',
    'Ложь и обман',
    'Отсутствие идеалов и принципов',
    'Отсутствие детей',
    'Отсутствие семьи и своего родного места (родительского дома)'
  ]
};

export const B_21: MatrixNumber = {
  num: 21,
  position: 'B',
  meaning_positive: [
    'Инновации',
    'Эксперименты',
    'Заграница и все иностранное',
    'Вызовы и преодоления',
    'Глобальность',
    'Разрыв шаблонов и общепринятых установок',
    'Разнообразие'
  ],
  meaning_negative: [
    'Предрассудки',
    'Ограничивающие убеждения',
    'Ограничения и установка границ возможного',
    'Узкое мышление',
    'Мелочность',
    'Быт (обыденность)',
    'Однообразие',
    'Отказ принимать чьи-то убеждения'
  ]
};

export const B_22: MatrixNumber = {
  num: 22,
  position: 'B',
  meaning_positive: [
    'Детские занятия (игры, игрушки, фантазии)',
    'Свобода',
    'Зов приключений и путешествий',
    'Веселье',
    'Наслаждение моментом',
    'Развлекательные мероприятия'
  ],
  meaning_negative: [
    'Попытки обмануть Вселенную',
    'Наивность и глупость',
    'Инфантильность',
    'Ограниченность обстоятельствами',
    'Несвобода',
    'Зависимости',
    'Криминал',
    'Недоверие',
    'Предрассудки'
  ]
};
