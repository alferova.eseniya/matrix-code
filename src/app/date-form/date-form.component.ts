import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-form',
  templateUrl: './date-form.component.html',
  styleUrls: ['./date-form.component.css']
})
export class DateFormComponent implements OnInit {
  birthDate: string | undefined;

  constructor() {}

  ngOnInit(): void {
    this.birthDate = '30-11-2001';
  }
}
